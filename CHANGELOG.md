# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.15](https://github.com/stewwan/firetask/compare/v0.2.14...v0.2.15) (2019-07-18)


### Bug Fixes

* changelog ([d02ebad](https://github.com/stewwan/firetask/commit/d02ebad))
* **firestore:** return a new state ([5e8a040](https://github.com/stewwan/firetask/commit/5e8a040))
* **rr:** move storage and connector out of options ([834d5d0](https://github.com/stewwan/firetask/commit/834d5d0))
* **state:** make it work using just 2 params ([44bc467](https://github.com/stewwan/firetask/commit/44bc467))


### Features

* **r:** add a way to retrieve states synchronously through rr api ([667d565](https://github.com/stewwan/firetask/commit/667d565))
* **rr:** add useState ([d420fdc](https://github.com/stewwan/firetask/commit/d420fdc))
* **rr:** introduce  the `silent` option ([bd92f52](https://github.com/stewwan/firetask/commit/bd92f52))
* **state:** add select ([1512fd0](https://github.com/stewwan/firetask/commit/1512fd0))
* **state:** move state getter/setter from rr ([1c83e29](https://github.com/stewwan/firetask/commit/1c83e29))
* **store:** add getState ([18bd10f](https://github.com/stewwan/firetask/commit/18bd10f))
* **store:** add setState ([6d3f0fe](https://github.com/stewwan/firetask/commit/6d3f0fe))



### [0.2.14](https://github.com/stewwan/firetask/compare/v0.2.13...v0.2.14) (2019-07-16)

### [0.2.13](https://github.com/stewwan/firetask/compare/v0.2.12...v0.2.13) (2019-07-16)

### Bug Fixes

- **rr:** diff function ([5617f62](https://github.com/stewwan/firetask/commit/5617f62))
- **state:** store was duplicating records ([3249049](https://github.com/stewwan/firetask/commit/3249049))
- **store:** again, make data param default to true for key method ([19ca350](https://github.com/stewwan/firetask/commit/19ca350))
- closes [#14](https://github.com/stewwan/firetask/issues/14) ([9a200fa](https://github.com/stewwan/firetask/commit/9a200fa))

### Features

- **rr:** add isOnline method and fix diff strategy ([0a4450d](https://github.com/stewwan/firetask/commit/0a4450d))
- **state:** merge elastic response detection ([0eae9a3](https://github.com/stewwan/firetask/commit/0eae9a3))
- **store:** make data param default to true ([00ee431](https://github.com/stewwan/firetask/commit/00ee431))
- **storybook:** add storysource plugin ([376dd65](https://github.com/stewwan/firetask/commit/376dd65))

### [0.2.12](https://github.com/stewwan/firetask/compare/v0.2.11...v0.2.12) (2019-07-15)

### Bug Fixes

- **state:** store was duplicating records ([3249049](https://github.com/stewwan/firetask/commit/3249049))

### [0.2.11](https://github.com/stewwan/firetask/compare/v0.2.10...v0.2.11) (2019-07-15)

### Bug Fixes

- **rr:** diff function ([5617f62](https://github.com/stewwan/firetask/commit/5617f62))

### Features

- **rr:** add isOnline method and fix diff strategy ([0a4450d](https://github.com/stewwan/firetask/commit/0a4450d))
- **state:** merge elastic response detection ([0eae9a3](https://github.com/stewwan/firetask/commit/0eae9a3))
- **store:** make data param default to true ([00ee431](https://github.com/stewwan/firetask/commit/00ee431))

## [0.2.10](https://github.com/stewwan/firetask/compare/v0.2.9...v0.2.10) (2019-07-15)

### Features

- **state:** merge elastic response detection ([0eae9a3](https://github.com/stewwan/firetask/commit/0eae9a3))

## [0.2.9](https://github.com/stewwan/firetask/compare/v0.2.7...v0.2.9) (2019-07-12)

### Features

- **rr:** add isOnline method and fix diff strategy ([0a4450d](https://github.com/stewwan/firetask/commit/0a4450d))

### [0.2.8](https://github.com/stewwan/firetask/compare/v0.2.7...v0.2.8) (2019-07-10)

### Bug Fixes

- **rr:** diff function ([5617f62](https://github.com/stewwan/firetask/commit/5617f62))

### Features

- **storybook:** add storysource plugin ([376dd65](https://github.com/stewwan/firetask/commit/376dd65))

### [0.2.7](https://github.com/stewwan/firetask/compare/v0.2.6...v0.2.7) (2019-06-25)

### Features

- **rr:** expose instance using `storage()` method ([825cc21](https://github.com/stewwan/firetask/commit/825cc21))

### [0.2.6](https://github.com/stewwan/firetask/compare/v0.2.5...v0.2.6) (2019-06-24)

### Bug Fixes

- **rr:** tests ([31f6782](https://github.com/stewwan/firetask/commit/31f6782))

### Features

- **rr:** implement use case `cache true` with `network false` ([cf5ee17](https://github.com/stewwan/firetask/commit/cf5ee17))
- **rr:** improve reactivity system ([dcf2bb7](https://github.com/stewwan/firetask/commit/dcf2bb7))

### [0.2.5](https://github.com/stewwan/firetask/compare/v0.2.4...v0.2.5) (2019-06-22)

### Bug Fixes

- **rr:** browser needs to merge options ([cd7457e](https://github.com/stewwan/firetask/commit/cd7457e))
- **rr:** driver initialization bug ([caebaa2](https://github.com/stewwan/firetask/commit/caebaa2))
- **rr:** now we have just a single `transform` fn in the chain for better readability ([9ab568c](https://github.com/stewwan/firetask/commit/9ab568c))
- **rr:** weird behaviors on browser platform when testing from a vue app ([cfe7b56](https://github.com/stewwan/firetask/commit/cfe7b56))

### [0.2.4](https://github.com/stewwan/firetask/compare/v0.2.3...v0.2.4) (2019-06-21)

### Bug Fixes

- libs initialization ([448b73e](https://github.com/stewwan/firetask/commit/448b73e))

### [0.2.3](https://github.com/stewwan/firetask/compare/v0.2.2...v0.2.3) (2019-06-21)

### Bug Fixes

- firetask internal dep ([e4a9c13](https://github.com/stewwan/firetask/commit/e4a9c13))

### [0.2.2](https://github.com/stewwan/firetask/compare/v0.2.1...v0.2.2) (2019-06-21)

### Bug Fixes

- edge case on deploy script and add travis to the newer packages ([c40a31e](https://github.com/stewwan/firetask/commit/c40a31e))

### [0.2.1](https://github.com/stewwan/firetask/compare/v0.2.0...v0.2.1) (2019-06-21)

### Bug Fixes

- update packages ([b93e8c0](https://github.com/stewwan/firetask/commit/b93e8c0))

## [0.2.0](https://github.com/stewwan/firetask/compare/v0.1.5...v0.2.0) (2019-06-21)

### Bug Fixes

- angular package, makes it more independent ([7310ae5](https://github.com/stewwan/firetask/commit/7310ae5))
- **rr:** firebase specs ([2a0c797](https://github.com/stewwan/firetask/commit/2a0c797))
- **rr:** move storage helper to the core package ([23c0b78](https://github.com/stewwan/firetask/commit/23c0b78))
- **rr:** update demo app to use latest packages ([0bc1b58](https://github.com/stewwan/firetask/commit/0bc1b58))
- **rr:** verb `on` for firebase should return an observable ([d2fa134](https://github.com/stewwan/firetask/commit/d2fa134))
- **rr:** was transforming response twice ([b27bfd6](https://github.com/stewwan/firetask/commit/b27bfd6))
- bump tool ([a587137](https://github.com/stewwan/firetask/commit/a587137))
- ionic package ([a5e75be](https://github.com/stewwan/firetask/commit/a5e75be))

### Features

- add new lib firebase ([5a24461](https://github.com/stewwan/firetask/commit/5a24461))
- add state package ([256ad1d](https://github.com/stewwan/firetask/commit/256ad1d))

### [0.1.5](https://github.com/stewwan/firetask/compare/v0.1.4...v0.1.5) (2019-06-19)

### Bug Fixes

- **rr:** response state ([b09206b](https://github.com/stewwan/firetask/commit/b09206b))

### [0.1.4](https://github.com/stewwan/firetask/compare/v0.1.3...v0.1.4) (2019-06-19)

### Bug Fixes

- **rr:** do not complete observable when useNetwork true and useCache false ([0a26956](https://github.com/stewwan/firetask/commit/0a26956))

### [0.1.3](https://github.com/stewwan/firetask/compare/v0.1.2...v0.1.3) (2019-06-18)

### Bug Fixes

- release script ([ff2e4e5](https://github.com/stewwan/firetask/commit/ff2e4e5))

### [0.1.2](https://github.com/stewwan/firetask/compare/v0.1.1...v0.1.2) (2019-06-18)

### Bug Fixes

- **ionic:** jest setup ([4fe0702](https://github.com/stewwan/firetask/commit/4fe0702))
- **rr:** fix specs ([788d2de](https://github.com/stewwan/firetask/commit/788d2de))

### Features

- add test setup to the other libs ([91f5aea](https://github.com/stewwan/firetask/commit/91f5aea))

### [0.1.1](https://github.com/stewwan/firetask/compare/v0.1.0...v0.1.1) (2019-06-17)

<a name="0.1.0"></a>

# 0.1.0 (2019-06-17)

### Bug Fixes

- **firebase:** check for object results ([dc8528f](https://github.com/stewwan/firetask/commit/dc8528f))
- **or:** remove hooks and deprecated apis completely ([4005630](https://github.com/stewwan/firetask/commit/4005630))
- **rr:** `on` verb wasnt working as expected and fixes [#8](https://github.com/stewwan/firetask/issues/8) [#9](https://github.com/stewwan/firetask/issues/9) ([75765a8](https://github.com/stewwan/firetask/commit/75765a8))
- **rr:** add a new exception for firebase and fix the old rr way ([59f024f](https://github.com/stewwan/firetask/commit/59f024f))
- **rr:** browser chaining and make sure to return response from network only if different from cache ([cc6ebb3](https://github.com/stewwan/firetask/commit/cc6ebb3))
- **rr:** browser platform should complete observer before set cache ([f9ae0aa](https://github.com/stewwan/firetask/commit/f9ae0aa))
- **rr:** build for prod ([f237aec](https://github.com/stewwan/firetask/commit/f237aec))
- **rr:** check for falsy ref on firebase driver ([963b4c6](https://github.com/stewwan/firetask/commit/963b4c6))
- **rr:** collection initialisation log ([ee5b22a](https://github.com/stewwan/firetask/commit/ee5b22a))
- **rr:** dispatch test ([7c3755d](https://github.com/stewwan/firetask/commit/7c3755d))
- **rr:** driver option for runtime executions ([5a90bc8](https://github.com/stewwan/firetask/commit/5a90bc8))
- **rr:** exception and start work of chrome extension ([eef8438](https://github.com/stewwan/firetask/commit/eef8438))
- **rr:** firebase connector ([45da78c](https://github.com/stewwan/firetask/commit/45da78c))
- **rr:** firebase/firestore connectors ([2e8b2dd](https://github.com/stewwan/firetask/commit/2e8b2dd))
- **rr:** firestore adjusts ([887ffcb](https://github.com/stewwan/firetask/commit/887ffcb))
- **rr:** http response ([0a6802a](https://github.com/stewwan/firetask/commit/0a6802a))
- **rr:** improve log trace ([eaeeeb1](https://github.com/stewwan/firetask/commit/eaeeeb1))
- **rr:** key creation strategy ([0897a2b](https://github.com/stewwan/firetask/commit/0897a2b))
- **rr:** move store stuff to firestask/angular module ([1d77c8f](https://github.com/stewwan/firetask/commit/1d77c8f))
- **rr:** observable wasnt running twice with cache ([4c26cfd](https://github.com/stewwan/firetask/commit/4c26cfd))
- **rr:** platform browser and improve logs ([cbff6c7](https://github.com/stewwan/firetask/commit/cbff6c7))
- **rr:** remove ngxs as a peer dep ([3a78237](https://github.com/stewwan/firetask/commit/3a78237))
- **rr:** remove shouldTransform ([4f46d20](https://github.com/stewwan/firetask/commit/4f46d20))
- **rr:** schematics ([340f2ba](https://github.com/stewwan/firetask/commit/340f2ba))
- cached responses ([cab7fe3](https://github.com/stewwan/firetask/commit/cab7fe3))
- fire drivers and add driver as a new prop in response metadata ([434f530](https://github.com/stewwan/firetask/commit/434f530))
- **rr:** verbs response type ([1de4e37](https://github.com/stewwan/firetask/commit/1de4e37))
- ionic release ([27a783f](https://github.com/stewwan/firetask/commit/27a783f))
- **rr:** storage and firestore driver for offline conditions ([5a6ce63](https://github.com/stewwan/firetask/commit/5a6ce63))
- package version ([885e9e1](https://github.com/stewwan/firetask/commit/885e9e1))
- **rr:** should return response immediately ([655b189](https://github.com/stewwan/firetask/commit/655b189))
- **rr:** some issues related with useCache false on browser ([f9f1774](https://github.com/stewwan/firetask/commit/f9f1774))
- **rr:** types ([35459e8](https://github.com/stewwan/firetask/commit/35459e8))
- **rr:** use a different name for `call` on browser ([a7fed76](https://github.com/stewwan/firetask/commit/a7fed76))
- **rr:** useCache on chaining ([b51b2c5](https://github.com/stewwan/firetask/commit/b51b2c5))
- **rr:** useNetwork yes and useCache yes should return two responses rather than just one ([a0b070b](https://github.com/stewwan/firetask/commit/a0b070b))
- **rr:** verb `on` from firestore driver was not adding key to the cache metadata ([cfa1a6b](https://github.com/stewwan/firetask/commit/cfa1a6b))
- **rr:** verify for network status in `on` verb ([5cd9c85](https://github.com/stewwan/firetask/commit/5cd9c85))
- **rr:** weird behaviors and improve internal logs ([ea494f3](https://github.com/stewwan/firetask/commit/ea494f3))
- **rr/firestore:** add experimentalTabSynchronization ([3be44bb](https://github.com/stewwan/firetask/commit/3be44bb))
- **rr/firestore:** driver log ([f74152b](https://github.com/stewwan/firetask/commit/f74152b))
- **rr/play:** chaining api ([15a9ed5](https://github.com/stewwan/firetask/commit/15a9ed5))
- **rr/play:** remove ts notation ([0d5ceac](https://github.com/stewwan/firetask/commit/0d5ceac))
- **ui:** say method should present internally ([cd6cf73](https://github.com/stewwan/firetask/commit/cd6cf73))

### Features

- **rr:** add feed method to fill store with cached responses on startup ([0774875](https://github.com/stewwan/firetask/commit/0774875))
- add release scripts ([48fb579](https://github.com/stewwan/firetask/commit/48fb579))
- add schematics for rr collection ([7f26c48](https://github.com/stewwan/firetask/commit/7f26c48))
- add some server specs ([099a679](https://github.com/stewwan/firetask/commit/099a679))
- **rr:** improve selector ([d7b5abd](https://github.com/stewwan/firetask/commit/d7b5abd))
- start workspace with the new rr lib ([5a13f65](https://github.com/stewwan/firetask/commit/5a13f65))
- **firestore:** add ability to cache for the `on` verb ([462e7e4](https://github.com/stewwan/firetask/commit/462e7e4))
- **ionic:** add say method to the ui ([382412c](https://github.com/stewwan/firetask/commit/382412c))
- **rr:** add a shortcut for transform data response ([891893c](https://github.com/stewwan/firetask/commit/891893c))
- **rr:** add CI and coverage ([cfade94](https://github.com/stewwan/firetask/commit/cfade94))
- **rr:** add clearCache to api ([128a330](https://github.com/stewwan/firetask/commit/128a330))
- **rr:** add global config ([876ac25](https://github.com/stewwan/firetask/commit/876ac25))
- **rr:** add logger ([bf884ec](https://github.com/stewwan/firetask/commit/bf884ec))
- **rr:** add more browser tests ([4712d56](https://github.com/stewwan/firetask/commit/4712d56))
- **rr:** add more tests for firestore driver ([c07e65d](https://github.com/stewwan/firetask/commit/c07e65d))
- **rr:** add more tests for firestore driver ([7bd7138](https://github.com/stewwan/firetask/commit/7bd7138))
- **rr:** add more types to storage adapter and expose to public api ([cb2a5e3](https://github.com/stewwan/firetask/commit/cb2a5e3))
- **rr:** add some specs for browser platform ([6a30ce8](https://github.com/stewwan/firetask/commit/6a30ce8))
- **rr:** add store reset ([7364122](https://github.com/stewwan/firetask/commit/7364122))
- **rr:** add tests for symbols ([cb2a09c](https://github.com/stewwan/firetask/commit/cb2a09c))
- **rr:** add the new chain option `diff` ([905d92f](https://github.com/stewwan/firetask/commit/905d92f))
- **rr:** add verb to cache key ([ffd825c](https://github.com/stewwan/firetask/commit/ffd825c))
- **rr:** complete firestore driver tests ([31596e0](https://github.com/stewwan/firetask/commit/31596e0))
- **rr:** complete unit tests for firebase driver \o/ ([05ce0f7](https://github.com/stewwan/firetask/commit/05ce0f7))
- **rr:** expose drivers ([b928727](https://github.com/stewwan/firetask/commit/b928727))
- **rr:** finish browser specs ([83b8eb6](https://github.com/stewwan/firetask/commit/83b8eb6))
- **rr:** finish drivers tests ([8d41f12](https://github.com/stewwan/firetask/commit/8d41f12))
- **rr:** finish server tests ([686b698](https://github.com/stewwan/firetask/commit/686b698))
- **rr:** implement store and key selector ([dd7e77e](https://github.com/stewwan/firetask/commit/dd7e77e))
- **rr:** improve traced log ([c7cbffc](https://github.com/stewwan/firetask/commit/c7cbffc))
- **rr:** make it fully initializable via angular ReactiveModule ([72ba484](https://github.com/stewwan/firetask/commit/72ba484))
- **rr:** move http stuff to a separate driver and add the possibility to use a custom http connector, like the angular http client ([7f785e4](https://github.com/stewwan/firetask/commit/7f785e4))
- **rr:** only feed the specific collection ([0d30c44](https://github.com/stewwan/firetask/commit/0d30c44))
- **rr:** reduce redundant code and apply the verb mapping ([e61e9e7](https://github.com/stewwan/firetask/commit/e61e9e7))
- **rr:** remove moment as dependency and add some tests for firestore driver ([bf8f83d](https://github.com/stewwan/firetask/commit/bf8f83d))
- **rr:** remove redundant code from browser and add some tests ([93ce228](https://github.com/stewwan/firetask/commit/93ce228))
- **rr:** uncouple old hooks from http. improve abstraction by separating concerns between browser x server ([2c52502](https://github.com/stewwan/firetask/commit/2c52502))
- **rr/play:** add basic real interaction using services ([34d7213](https://github.com/stewwan/firetask/commit/34d7213))
- **rr/play:** add browser/server tabs ([ea3361c](https://github.com/stewwan/firetask/commit/ea3361c))
- **rr/play:** add cache explorer ([38fa752](https://github.com/stewwan/firetask/commit/38fa752))
- **rr/play:** add cache-explorer-container ([ff76685](https://github.com/stewwan/firetask/commit/ff76685))
- **rr/play:** add chain verb and wip cache explorer ([730a981](https://github.com/stewwan/firetask/commit/730a981))
- **rr/play:** add collection chooser ([2f00a23](https://github.com/stewwan/firetask/commit/2f00a23))
- **rr/play:** add collection scheme container ([3789bc7](https://github.com/stewwan/firetask/commit/3789bc7))
- **rr/play:** add collections ([e8a7920](https://github.com/stewwan/firetask/commit/e8a7920))
- **rr/play:** add initial working structure with 3 field types ([8f9c58c](https://github.com/stewwan/firetask/commit/8f9c58c))
- **rr/play:** add json tree viewer ([16b2ebf](https://github.com/stewwan/firetask/commit/16b2ebf))
- **rr/play:** add log card ([052ee07](https://github.com/stewwan/firetask/commit/052ee07))
- **rr/play:** add log methods ([2aec780](https://github.com/stewwan/firetask/commit/2aec780))
- **rr/play:** add transformCache method ([7a95e24](https://github.com/stewwan/firetask/commit/7a95e24))
- transform data from elastic ([7a5e411](https://github.com/stewwan/firetask/commit/7a5e411))
- **rr/play:** implement select field ([168b670](https://github.com/stewwan/firetask/commit/168b670))
- **rr/play:** make browser/server tabs work ([c8d933f](https://github.com/stewwan/firetask/commit/c8d933f))
- **rr/play:** refactor to ngxs ([6977b2a](https://github.com/stewwan/firetask/commit/6977b2a))
- **rr/play:** verb chooser separated into its own component ([af5c73a](https://github.com/stewwan/firetask/commit/af5c73a))

### Performance Improvements

- **rr:** clean up code by removing `request`stuff and fixes browser platform related issues ([c225bae](https://github.com/stewwan/firetask/commit/c225bae))
