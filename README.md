# Firetask Core [![Build Status](https://travis-ci.org/Firetask/core.svg?branch=master)](https://travis-ci.org/Firetask/core) [![codecov](https://codecov.io/gh/Firetask/core/branch/master/graph/badge.svg)](https://codecov.io/gh/Firetask/core) [![Conventional Commits](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](https://conventionalcommits.org)

> Attention
>
> This repository is destinated to run the CI/CD stuff. For issues and pull requests please use the main repo [`Firestask`](https://github.com/firetask/firetask)

## Docs

Coming soon

## License

MIT
