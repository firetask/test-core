# Reactive Record

**Reactive Record** allows you to build your app really fast with a lot of fun. Its concept is based on reactive programming but it doesn't mean you're going to be stuck on redux patterns. You can work with it using **Observables**, **Promises** or even its built-in mini **Store** powered by NGXS.

The lib is available on server side and client side. However in browser it has a lot of cool features such as the cache and state management. You can use the old but gold vanilla.js or take advantage of typescript to level up your code integrity/quality while you reduce the boilerplate.

You are in good hands. Every verb call returns an Observable which means you can do anything with it. The chaining api is clear and easy to remind. Besides unit tests RR is also tested across multiple apps before each release.

