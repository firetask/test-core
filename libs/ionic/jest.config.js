module.exports = {
  name: 'ionic',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/ionic',
  transformIgnorePatterns: ['node_modules/(?!@ionic)']
};
