export type ReactiveVerb =
  | 'get'
  | 'post'
  | 'patch'
  | 'delete'
  | 'find'
  | 'findOne'
  | 'set'
  | 'update'
  | 'on';
